# Теперь нужно нарисовать змейку. Змейка - это список таких же ячеек,
# только зеленых. Рисовать одну ячейку мы уже умеем. Справитесь со списком?

import pygame


WINDOW_WIDTH = 640
WINDOW_HEIGHT = 480

# Фиксируем размер ячейки
CELL_SIZE = 20

# Цвета
BG_COLOR = (0, 0, 0)
GRID_COLOR = (40, 40, 40)
# Яблоко будет самым красным!
APPLE_COLOR = (255, 0, 0)
# А змея - самой зеленой!
SNAKE_COLOR = (0, 255, 0)

FPS = 15


class Cell:
    def __init__(self, x, y):
        self.x = x
        self.y = y


def main():
    global FPS_CLOCK
    global DISPLAY

    pygame.init()
    FPS_CLOCK = pygame.time.Clock()
    DISPLAY = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
    pygame.display.set_caption('Wormy')

    while True:
        # Мы всегда будем начинать игру с начала. После проигрыша сразу
        # начинается следующая.
        run_game()


def run_game():
    apple = Cell(20, 10)

    # Змея - просто список ячеек. Расположим змею на той же строке,
    # что и яблоко, чтобы она жадно смотрела и облизывалась.
    snake = [
        Cell(5, 10),
        Cell(4, 10),
        Cell(3, 10)]

    # А вот это тот самый игровой цикл
    while True:
        # TODO: обработать пользовательский ввод
        # TODO: рассчитать физику: столкновения, новые позиции змейки и яблока

        # Функция рисования кадра уже знает, что нужно рисовать яблоко.
        # Давайте скажем ей, что у нас есть еще и змея.
        draw_frame(snake, apple)
        FPS_CLOCK.tick(FPS)


def draw_frame(snake, apple):
    DISPLAY.fill(BG_COLOR)
    draw_grid()
    draw_snake(snake)
    draw_apple(apple)
    pygame.display.update()


def draw_grid():
    for x in range(0, WINDOW_WIDTH, CELL_SIZE):
        pygame.draw.line(DISPLAY, GRID_COLOR, (x, 0), (x, WINDOW_HEIGHT))
    for y in range(0, WINDOW_HEIGHT, CELL_SIZE):
        pygame.draw.line(DISPLAY, GRID_COLOR, (0, y), (WINDOW_WIDTH, y))


def draw_apple(apple):
    draw_cell(apple, APPLE_COLOR)


def draw_snake(snake):
    for cell in snake:
        # Ну вот, пригодилась
        draw_cell(cell, SNAKE_COLOR)


def draw_cell(cell, color):
    x = cell.x * CELL_SIZE
    y = cell.y * CELL_SIZE
    rect = pygame.Rect(x, y, CELL_SIZE, CELL_SIZE)
    pygame.draw.rect(DISPLAY, color, rect)


if __name__ == '__main__':
    main()
